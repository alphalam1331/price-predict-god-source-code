import { Router, Request, Response } from "express";
import { Stock_Service } from "./stock_service";

export class Stock_Controller {
    router = Router()

    constructor(private stock_Service: Stock_Service) {
        this.router.get('/symbol', this.load_all_symbols)
        this.router.get('/stock/:symbol', this.load_stock_data)
    }
    load_all_symbols = async (req: Request, res: Response) => {
        try {
            let symbol_list = await this.stock_Service.load_all_symbols()
            res.status(200).json({ message: 'The data is Successfully loaded.', data: symbol_list})

        } catch(error){
            res.status(500).json(error)
        }
    }

    load_stock_data = async(req: Request, res: Response) => {
        try {
            let {symbol} = req.params
            // console.log ('symbol: ', symbol)
            let stock_data = await this.stock_Service.load_stock_data(symbol)

            // console.log ('Stock_data: ', stock_data)
            res.status(200).json({ message: 'The data is Successfully loaded.', data: stock_data})
        } catch (error) {
            res.status(500).json(error)
        }
    }
    TODO = (req: Request, res: Response) => {
        console.log("Coming Soon")
    }
}