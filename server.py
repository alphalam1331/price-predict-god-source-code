from sanic import Sanic
from sanic.response import json
import tensorflow as tf
import numpy as np
app = Sanic("Price_predict")

@app.route("/")
def test(request):
    return json({"hello": "world"})

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000)