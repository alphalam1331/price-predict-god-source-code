import { Knex } from "knex";
import { Pattern } from "./type";

export class Pattern_Service {
    constructor(private knex: Knex) { }

    load_all_patterns = async () => {
        try {
            let pattern_list = await this.knex('patterns').select('pattern_name')
            return pattern_list
        } catch (error) {
            return Error('failed to load symbol.')
        }
    }
    submit_pattern = async (picked_pattern: Pattern) => {
        let { symbol_name, date1, date2, pattern_label } = picked_pattern
        const data = await this.knex.with('data', (qb) => {
            qb.select('*').from('stocks_data').whereIn('date', [date1, date2])
        }).select('id').from('data').where ('symbol', symbol_name)

        const pattern_id = (await this.knex('patterns').select('id').where('pattern_name', pattern_label))[0].id
        await this.knex('patterns_data').insert(
            {
                pattern_id
                , start_point: data[0].id
                , end_point: data[1].id
            }
        )  
        console.log('New Pattern sample...: ')
        console.log(picked_pattern)
        return picked_pattern
    }
}