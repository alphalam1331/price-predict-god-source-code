import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if (!(await knex.schema.hasTable('stocks_data'))) {
        await knex.schema.createTable('stocks_data', (t) => {
            t.increments('id')
            t.string('symbol').references('stocks.symbol')
                , t.date('date')
                , t.float('open')
                , t.float('high')
                , t.float('low')
                , t.float('close')
                , t.float('adj_close')
                , t.bigInteger('volume')
        })
    }
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('stocks_data')
}

