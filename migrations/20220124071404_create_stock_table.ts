import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if (!(await knex.schema.hasTable('stocks'))){
        await knex.schema.createTable('stocks', (t)=>{
            t.string('symbol', 10).primary()
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('stocks')
}

