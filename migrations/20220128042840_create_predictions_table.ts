import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if (!(await knex.schema.hasTable('models'))) {
        await knex.schema.createTable('models', (t) => {
            t.string('name', 10).primary()
        })
    }
    if (!(await knex.schema.hasTable('predictions'))) {
        await knex.schema.createTable('predictions', (t) => {
            t.increments()
            t.date('date')
            t.string('symbol', 10).index()
            t.float('adj_close')
            t.float('predict_price')
            t.string('model', 10).index()
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('models')
    await knex.schema.dropTableIfExists('predictions')
}

