import { Knex } from "knex";
import { predictions_path, prediction_list} from "../process_data";
import { readFile, utils } from 'xlsx'
import { join } from 'path';
import { format } from 'date-fns'
import { Prediction } from "../type";


export async function up(knex: Knex): Promise<void> {
    for (let item of prediction_list) {
        let wb = readFile(join(predictions_path, `${item[0]}_${item[1]}_predict.csv`), { cellDates: true })
        let sheet_name = wb.SheetNames[0]
        let data: Prediction[] = utils.sheet_to_json(wb.Sheets[sheet_name])

        if (!(await knex('models').select().where('name', item[1]))) {
            await knex('models').insert({ 'name': item[1] })
        }
        console.log (item)
        for (let row of data) {
            await knex("predictions").insert(
                {
                    symbol: item[0]
                    , date: row['Date'] = row['Date'] ? format(new Date(row['Date']), 'yyyy-MM-dd') : ''
                    , adj_close: row['Adj Close'] = row['Adj Close'] ? +row['Adj Close'].toFixed(2) : NaN
                    , predict_price: row['Predictions'] = row['Predictions'] ? +row['Predictions'].toFixed(2) : NaN
                    , model: item[1]
                },
            );
        }
    }
}

    export async function down(knex: Knex): Promise<void> {
        await knex('predictions').truncate()
        await knex('models').truncate()
}

