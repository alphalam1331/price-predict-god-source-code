import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if (!(await knex.schema.hasTable('patterns'))) {
        await knex.schema.createTable('patterns', (t) => {
            t.increments('id')
            t.string('pattern_name', 255)
        })
    }
    await knex('patterns').insert({pattern_name: 'Support Level'})
    
    if (!(await knex.schema.hasTable('patterns_data'))) {
        await knex.schema.createTable('patterns_data', (t) => {
            t.increments('id')
            t.integer('pattern_id').references('patterns.id')
            t.integer('start_point').references('stocks_data.id')
            t.integer('end_point').references('stocks_data.id')
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('patterns')
    await knex.schema.dropTableIfExists('patterns_data')
}

