import {config as Config} from 'dotenv'

Config()

export const env = {
    HOST: process.env.HOST! ||'localhost',
    PORT: +process.env.HOST! ||8080,
    DB_NAME:process.env.DB_NAME,
    DB_USER:process.env.DB_USER,
    DB_PASSWORD:process.env.DB_PASSWORD,
    NODE_ENV: process.env.MY_NODE_ENV
}
