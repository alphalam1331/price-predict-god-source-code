import {knex }from './knex';
import {Stock_Service} from './stock_service'

let stock_service: Stock_Service;

describe ('testing stock service', ()=>{
    beforeAll(()=>{
        stock_service = new Stock_Service(knex)        
    })

    afterAll(()=>{
        knex.destroy()
    })
    
    test('load stock data from database', async ()=>{
        let files = await stock_service.load_stock_data('AAPL')
        console.log (files)
    })

    test.only('array slice', ()=>{
        let arr = [1,2,3,4,5,6,7,8,9,10]
        let arr2 = arr.slice(0,5)
        console.log (arr)
        console.log (arr2)
    })
})
