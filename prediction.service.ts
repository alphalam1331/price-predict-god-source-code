import { Knex } from "knex";

export class Prediction_Service {
    constructor(private knex: Knex) { }
    table= ()=>{
        return this.knex('predictions')
    }
    load_prediction = async(symbol:string, date:string) => {
        try {
            const data = await this.table().select('date', 'predict_price', 'model').where({'symbol': symbol}).andWhere('date','>', date).orderBy('date', 'asc')
            
            console.log (date)
            console.log (data)
            
            return data
        } catch (error) {
            return `failed to load from Prediction Table:  ${error}`
        }
    }

}