import { Prediction_Service } from "./prediction.service";
import { knex } from "./knex";

describe('Prediction', ()=>{
    let prediction_service: Prediction_Service

    beforeAll(()=>{
        prediction_service = new Prediction_Service(knex)
    })
    afterAll(()=>{
        knex.destroy()
    })
    
    test('load AAPL data from Database', async()=>{
        let data = await prediction_service.load_prediction('F', '2018-6-28')
        console.log (data)
    })

})