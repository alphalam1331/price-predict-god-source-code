function setup_animation(data) {
    const totalDuration = 10000;
    const delayBetweenPoints = 10000 / 10000;
    function easeIn(t){
        return t * t * t * t * t *t * t
    }
    const previousY = (ctx) => {
        // if (ctx.index <=2) {
        //     if (ctx.index === 0) {
        //         console.log ('index === 0')
        //         console.log (ctx.chart.getDatasetMeta(ctx.datasetIndex).data[ctx.index].y)
        //     }
        //     if (ctx.index === 1) {
        //         console.log ('index === 1')
        //         console.log (ctx.chart.getDatasetMeta(ctx.datasetIndex).data[ctx.index].$context.parsed.y)
        //     }else if (ctx.index === 2) {
        //         console.log ('index === 2')
        //         console.log (ctx.chart.getDatasetMeta(ctx.datasetIndex).data[ctx.index].$context.parsed.y)
        //     }
        // }
        ctx.index === 0 ? 10 : 12
        // ctx.index === 0 ? ctx.chart.getDatasetMeta(ctx.datasetIndex).data[ctx.index].y: ctx.chart.getDatasetMeta(ctx.datasetIndex).data[ctx.index-1].y
        // ctx.index === 0 ? ctx.chart.scales.y.getPixelForValue(100) : ctx.chart.getDatasetMeta(ctx.datasetIndex).data[ctx.index - 1].getProps(['y'], true).y;
    }

    return {
        x: {
            type: 'number',
            easing: 'linear',
            duration: delayBetweenPoints,
            from: '2010-4-21', // the point is initially skipped
            delay(ctx) {
                if (ctx.type !== 'data' || ctx.xStarted) {
                    return 0;
                }
                ctx.xStarted = true;
                return ctx.index * delayBetweenPoints;
            }
        },
        y: {
            type: 'number',
            easing: 'linear',
            duration: delayBetweenPoints,
            from: NaN,
            delay(ctx) {
                if (ctx.type !== 'data' || ctx.yStarted) {
                    return 0;
                }
                ctx.yStarted = true;
                return ctx.index * delayBetweenPoints;
            }
        }
    }
}