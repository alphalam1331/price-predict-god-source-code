const canvas = document.querySelector('#main_canvas')
const drop_down_template = document.querySelector('#drop-down-template')
const nav_bar = document.querySelector('nav')
const header_name = document.querySelector('#page_name')
const pick_data_form = nav_bar.querySelector('#picked-data')
const pick_data_tags = pick_data_form.querySelectorAll('.tag')


let symbol_list = []
let config
let main_chart
let current_symbol

//initialize webpage and canvas
if (nav_bar) {
    gen_symbol_list()
        .then((res) => {
            get_stock_data(symbol_list[0])
                .then((stock_data) => {
                    config = setup_config(stock_data, symbol_list[0])
                    config.options.plugins.scales.y.ticks.min = Math.round(Math.min.apply(this, config.data.datasets[0].data) - 5)
                    config.options.plugins.scales.y.ticks.max = Math.round(Math.max.apply(this, config.data.datasets[0].data) + 5)
                    main_chart = new Chart(canvas, config)
                })
        })
}


//listen to the chosen stock 
nav_bar.addEventListener('click', (e) => {
    if (e.target.matches('#stock-list')) {
        e.target.addEventListener('change', (e) => {
            let name = e.target.value
            console.log(name)
            get_stock_data(name)
                .then((stock_data) => {
                    let { close_price, date } = stock_data
                    main_chart.data.labels = date
                    main_chart.data.datasets[0].label = name
                    main_chart.data.datasets[0].data = close_price
                    main_chart.update()
                    main_chart.resetZoom(mode = 'none')
                })
        })
    }
})


//listen to the pick-data-form
pick_data_form.addEventListener('submit', submit_pattern)


// functions related to Symbol

async function gen_symbol_list() {
    try {
        let result = await fetch('/symbol')
        let symbols = (await result.json()).data
        let menu = drop_down_template.content.cloneNode(true)
        let list = menu.querySelector('#stock-list')
        for (let symbol of symbols) {
            let name = symbol['symbol']

            let item = document.createElement('option')
            item.value = name
            item.textContent = name
            list.append(item)
            

            symbol_list.push(name)
        }
        // header_name.after(menu)
        // nav_bar.prepend(menu)
        return
    } catch (error) {
        console.log(error)
        return
    }
}

async function get_stock_data(stock_name) {
    try {
        current_symbol = stock_name
        let result = await fetch(`/stock/${stock_name}`)
        let stock_data = (await result.json()).data
        // console.log(stock_data)
        // let {stock_name} = stock_data
        let close_price = []
        let date = []
        for (let row of stock_data.stock_data) {
            close_price.push(row.adj_close)

            let raw_date = new Date(row.date)
            let formatted_date = raw_date.getFullYear() + '-' + raw_date.getMonth() + '-' + raw_date.getDate()
            date.push(formatted_date)
        }
        console.log({ date, close_price })
        return { close_price, date }
    } catch (error) {
        console.log(error)
        return
    }
}


// functions related to Pattern

async function get_pattern_list() {

}

async function submit_pattern(event) {
    event.preventDefault()
    const symbol_name = current_symbol
    const date1 = pick_data_form.date1.value
    const price1 = pick_data_form.price1.value
    const date2 = pick_data_form.date2.value
    const price2 = pick_data_form.price2.value
    const pattern_label = pick_data_form.pattern_label.value

    const picked_pattern = { symbol_name, date1, price1, date2, price2, pattern_label }

    let res = await fetch(pick_data_form.action, {
        method: pick_data_form.method,
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(picked_pattern)
    })
    let result = await res.json()
    console.log(result.data)
    // console.log(date1)
    // console.log(price1)
    // console.log(date2)
    // console.log(price2)
    // console.log(pattern)
}
