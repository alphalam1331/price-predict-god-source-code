const grid_color = 'rgb(100,100,100)'
const text_color = 'rgb(55, 55, 55)'
const line_color = 'rgb(100,120,120,0.5)'
const filled_color = 'rgb(168, 168, 124, 0.2)'

const rangeSelector = {
    buttons: [{
        count: 1,
        type: 'day',
        text: '1d'
    }, {
        count: 5,
        type: 'day',
        text: '5d'
    }, {
        count: 1,
        type: 'month',
        text: '1m'
    }, {
        count: 3,
        type: 'month',
        text: '3m'
    }, {
        count: 1,
        type: 'year',
        text: '1y'
    }, {
        count: 6,
        type: 'year',
        text: '6y'
    }, {
        type: 'all',
        text: 'All'
    }],
    inputEnabled: false,
    selected: 3
}


function setup_config(stock_data, stock_name) {
    return {
        chart: {
            height: 800
        },
        rangeSelector,
        legend: {
            enabled: true,
            align: 'center',
            verticalAlign: 'top',
            borderWidth: 0
        },
        yAxis: {
            crosshair: true,
            title: {
                text: 'Price'
            },
            labels: {
                formatter: function () {
                    return 'USD ' + this.value;
                }
            },
        },

        xAxis: {
            crosshair: true,
            title: {
                text: 'Date'
            }
        },
        tooltip: {
            shared: true,
            crosshair: true,
            formatter: function () {
                return this.points.reduce(function (s, point) {
                    return `${s}<br/>Series: <b>${point.series.name}</b><br/>Value: <b>USD ${point.y}</b><br/>`
                }, `Date: <b>${format_date(new Date(this.x))}</b>`);
            },
            //     return ;
        },

        exporting: {
            enabled: true
        },

        series: [{
            type: 'line',
            id: stock_name,
            name: stock_name,
            data: stock_data,
            tooltip: {
                valueDecimals: 2
            }
        }],


    }
}

function dynamic_load(series, data, time = 1000) {
    // set up the updating of the chart each second
    // console.log(series)
    return setInterval(function () {
        if (data.length<=0){
            return
        }
        let xy = data.shift()
        let x = xy[0]
        let y = xy[1]
        series.addPoint(xy, redraw = true, shift = true);
        
        // console.log (data.length)
        // console.log (xy)
    }, time);
}

function gen_color(model) {
    let r = model == "LSTM" ? 255 : Math.random() * 125
    let g = model == "GAN" ? 255 : Math.random() * 125
    let b = Math.random() * 255
    return `rgb(${r},${g},${b},0.4)`
}

