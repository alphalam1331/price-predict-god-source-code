const grid_color = 'rgb(100,100,100)'
const text_color = 'rgb(55, 55, 55)'

const label_options = {
    x: {
        title: {
            display: true,
            text: 'Date'
        }
    },
    y: {
        title: {
            display: true,
            text: 'Price'
        }
    }
}

const scale_options = {
    x: {
        type: 'category',
        title: {
            display: true,
            text: 'Date'
        },
        grid: {
            color: grid_color
        },
        ticks: {
            color: text_color
        }
    },
    y: {
        title: {
            display: true,
            text: 'Price'
        },
        grid: {
            color: grid_color
        },
        ticks: {
            color: text_color,
            // min: Math.round(Math.min.apply(this, config.data.datasets[0].data) - 5),
            // max: Math.round(Math.max.apply(this, config.data.datasets[0].data) + 5),
            stepSize: 5
        },
    }
}

const zoomOptions = {
    pan: {
        enabled: true,
        mode: 'x',
    },
    zoom: {
        wheel: {
            enabled: true,
        },
        pinch: {
            enabled: true
        },
        drag: {
            enabled: true,
            modifierKey: 'shift',
        },
        mode: 'x',
        onZoomComplete({ chart }) {
            // This update is needed to display up to date zoom level in the title.
            // Without this, previous zoom level is displayed.
            // The reason is: title uses the same beforeUpdate hook, and is evaluated before zoom.
            chart.update('none');
        }
    }
};

const interationOption = {
    intersect: false,
    axis: 'x',
    mode: 'nearest',
}




// const borderPlugin = {
//     id: 'panAreaBorder',
//     beforeDraw(chart, args, options) {
//         const { ctx, chartArea: { left, top, width, height } } = chart;
//         ctx.save();
//         ctx.strokeStyle = 'rgba(255, 0, 0, 0.3)';
//         ctx.lineWidth = 1;
//         ctx.strokeRect(left + width * 0.25, top + height * 0.25, width / 2, height / 2);
//         ctx.restore();
//     }
// };

let annotationOptions = {
    annotations: {
        line1: {
            // Indicates the type of annotation
            type: 'line',
            xMin: 0,
            xMax: 0,
            borderColor: 'rgba(255, 0, 0, 0.25)'
        },
        line2: {
            type: 'line',
            xMin: 0,
            xMax: 0,
            borderColor: 'rgba(0, 255, 100, 0.25)'
        },
        background: {
            type: 'box',
            xMin: 0,
            xMax: 0,
            backgroundColor: 'rgba(250, 250, 0, 0.4)',
            borderWidth: 0,
            borderRadius: 0,
        }
    }
}

function get_price(e) {
    const chart = e.chart
    
    let index = chart.tooltip.dataPoints[0].parsed.x
    
    let current_pair;

    const first_pair = document.querySelector('#picked-data>:first-child')
    const second_pair = document.querySelector('#picked-data>:nth-child(2)')

    if (first_pair.classList.contains("next")) {
        first_pair.classList.toggle('next')
        second_pair.classList.toggle('next')
        config.options.plugins.annotation.annotations.line1.xMin = index
        config.options.plugins.annotation.annotations.line1.xMax = index
        config.options.plugins.annotation.annotations.background.xMin = index
        current_pair = first_pair

    } else {
        first_pair.classList.toggle('next')
        second_pair.classList.toggle('next')
        config.options.plugins.annotation.annotations.line2.xMin = index
        config.options.plugins.annotation.annotations.line2.xMax = index
        config.options.plugins.annotation.annotations.background.xMax = index
        current_pair = second_pair
    }

    date_tag = current_pair.querySelector(':first-child')
    price_tag = current_pair.querySelector(':last-child')

    const date = chart.tooltip.dataPoints[0].label;
    const price = chart.tooltip.dataPoints[0].raw.toFixed(2);

    date_tag.value = `${date}`
    price_tag.value = `${price}`
    chart.update()
    //checking the information from click event
    // console.log(chart.tooltip.dataPoints[0])
    //This is the Xindex of the click point
    // console.log(chart.tooltip.dataPoints[0].parsed.x)
    //Function to update the vertical line
    // updateAnnotation(chart, chart.tooltip.dataPoints[0].parsed.x)
}



function setup_config(stock_data, stock_name) {
    const data = {
        labels: stock_data.date,
        datasets: [{
            label: stock_name,
            backgroundColor: 'rgb(235, 235 , 235)',
            borderColor: grid_color,
            borderWidth: 0.5,
            pointRadius: 0,
            data: stock_data.close_price,
            fill: true
        }]
    }

    return config = {
        type: 'line',
        data: data,
        options: {
            scales: label_options,
            interaction: interationOption,
            onClick: get_price,
            plugins: {
                zoom: zoomOptions,
                legend: true,
                scales: scale_options,
                autocolors: false,
                annotation: annotationOptions
            }
        },
        // plugins: [borderPlugin]
    }
}

