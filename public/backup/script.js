const canvas = document.querySelector('#main_canvas')
const nav_bar = document.querySelector('nav')
const drop_down_template = document.querySelector('#drop-down-template')


let symbol_list = []
let config
let main_chart
let current_symbol

//initialize webpage and canvas
if (nav_bar) {
    gen_symbol_list()
        .then((res) => {
            get_stock_data(symbol_list[0])
                .then((stock_data) => {
                    config = setup_config(stock_data, symbol_list[0])
                    config.options.plugins.scales.y.ticks.min = Math.round(Math.min.apply(this, config.data.datasets[0].data) - 5)
                    config.options.plugins.scales.y.ticks.max = Math.round(Math.max.apply(this, config.data.datasets[0].data) + 5)
                    main_chart = new Chart(canvas, config)
                    console.log (main_chart.tooltip )
                })
        })
}

console.log (Chart.helpers.easingEffects)

//listen to the chosen stock 
nav_bar.addEventListener('click', (e) => {
    if (e.target.matches('#stock-list')) {
        e.target.addEventListener('change', (e) => {
            let name = e.target.value
            console.log(name)
            get_stock_data(name)
                .then((stock_data) => {
                    let  {close_price, date } = stock_data
                    main_chart.data.labels = date
                    main_chart.data.datasets[0].label = name
                    main_chart.data.datasets[0].data = close_price
                    main_chart.update()
                    main_chart.resetZoom(mode = 'none')
                })
        })
    }
})




// functions related to Symbol

async function gen_symbol_list() {
    try {
        let result = await fetch('/symbol')
        let symbols = (await result.json()).data
        let menu = drop_down_template.content.cloneNode(true)
        let list = menu.querySelector('#stock-list')
        for (let symbol of symbols) {
            let name = symbol['symbol']

            let item = document.createElement('option')
            item.value = name
            item.textContent = name
            list.append(item)

            symbol_list.push(name)
        }
        nav_bar.prepend(menu)
        return
    } catch (error) {
        console.log(error)
        return
    }
}

async function get_stock_data(stock_name) {
    try {
        current_symbol = stock_name
        let result = await fetch(`/stock/${stock_name}`)
        let stock_data = (await result.json()).data
        // console.log(stock_data)
        // let {stock_name} = stock_data
        let close_price = []
        for (let row of stock_data.stock_data) {
            close_price.push(row.adj_close)
        }
        let date = []
        for (let row of stock_data.stock_data) {
            let raw_date = new Date(row.date)
            let formatted_date = raw_date.getFullYear() + '-' + +(raw_date.getMonth()+1) + '-' + raw_date.getDate()
            date.push(formatted_date)
        }
        console.log({ date, close_price })
        return { close_price, date }
    } catch (error) {
        console.log(error)
        return
    }
}