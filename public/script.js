
const nav_bar = document.querySelector('nav')
const top_header = document.querySelector('header')
const header_name = document.querySelector('#page_name')
const drop_down_template = document.querySelector('#drop-down-template')
const buy_button = document.querySelector('#buy_button')
const sell_button = document.querySelector('#sell_button')
const tradesList = document.querySelectorAll('.trade')
const user_record = document.querySelector('#user_record')
const tbody = document.querySelector('#user_record tbody')

let restartData = []
let condition = 'waiting'
let numberOfTrades = 1
let speed = 1000
let intervalCheck = true
let main_chart
let current_symbol
let lastPrice
let interval_list = []
let predicted = false
let LSTMTradenumber = 1
let GANTradenumber = 1

// initialize webpage and canvas
if (top_header) {
    load_symbol_list()
        .then((res_symbol_list) => {
            let symbol_list = insert_symbol_list(res_symbol_list)
            let rand_num = Math.floor(Math.random() * (symbol_list.length - 1))
            current_symbol = symbol_list[rand_num]
            top_header.querySelector('#stock-list').value = current_symbol
            return get_stock_data(current_symbol)
        }).then((stock_data) => {
            let { close_price, date, test_close_price, test_date } = stock_data
            stock_data = restructure_data(close_price, date)
            let config = setup_config(stock_data, current_symbol)

            main_chart = Highcharts.stockChart('main_chart', config);

            let test_data = restructure_data(test_close_price, test_date)

            // let series = main_chart.series[0]
            // let data = test_data

            // series_arr.push({ series, data })

            main_chart.series[0].chart.eventOptions = dynamic_load(main_chart.series[0], test_data)
            interval_list.push(main_chart.series[0].chart.eventOptions)
            restartData.push(test_data)
            // console.log(restartData)
        });;
}

//reset default value function
function resetDefaultValue() {
    condition = 'waiting'
    numberOfTrades = 1
    intervalCheck = true
    speed = 1000
    predicted = false
    LSTMTradenumber = 1
    GANTradenumber = 1
    while (tbody.firstChild) {
        tbody.removeChild(tbody.firstChild);

    }
    document.querySelector('#LSTM_record tbody').innerHTML = ""
    document.querySelector('#GAN_record tbody').innerHTML = ""
}

//show current stock price
setInterval(() => {
    let today = main_chart.series[0].processedXData.slice(-1)
    document.querySelector('#today').innerHTML = format_date(new Date(+today))
    // console.log(main_chart.series)
    let close = (main_chart.series[0].processedYData.slice(-1))
    if (main_chart) {
        if (lastPrice < close) {
            document.querySelector('#current_price').style.backgroundColor = 'greenyellow'
        } else if (lastPrice > close) {
            document.querySelector('#current_price').style.backgroundColor = 'rgb(220, 135, 153)'
        } else {
            document.querySelector('#current_price').style.color = 'black'
            document.querySelector('#current_price').style.backgroundColor = 'transparent'
        }
        document.querySelector('#current_price').innerHTML = close
        lastPrice = parseFloat(main_chart.series[0].processedYData.slice(-1))
    }
}, 500);

document.querySelector('#pause').addEventListener('click', () => {
    if (intervalCheck === false) {
        return
    }
    intervalCheck = false
    for (let i of interval_list) {
        clearInterval(i)
    }

})
//restart interval after clearInterval
document.querySelector('#play').addEventListener('click', () => {
    if (intervalCheck === true) {
        return
    }
    intervalCheck = true
    for (let i = 0; i < main_chart.series.length; i++) {
        main_chart.series[i].chart.eventOptions = dynamic_load(main_chart.series[i], restartData[i], speed)
        interval_list.push(main_chart.series[i].chart.eventOptions)
    }
})
//data looping speed slower
document.querySelector('#slower').addEventListener('click', () => {
    if (intervalCheck === false || speed >= 1600) {
        // console.log('slower speed: ', speed)
        return
    }
    for (let i of interval_list) {
        clearInterval(i)
    }
    speed = speed + 200
    for (let i = 0; i < main_chart.series.length; i++) {
        main_chart.series[i].chart.eventOptions = dynamic_load(main_chart.series[i], restartData[i], speed)
        interval_list.push(main_chart.series[i].chart.eventOptions)
    }
})
//data looping speed faster
document.querySelector('#faster').addEventListener('click', () => {
    if (intervalCheck === false || speed <= 400) {
        // console.log('faster speed: ', speed)
        return
    }
    speed = speed - 200
    for (let i of interval_list) {
        clearInterval(i)
    }

    for (let i = 0; i < main_chart.series.length - 1; i++) {
        main_chart.series[i].chart.eventOptions = dynamic_load(main_chart.series[i], restartData[i], speed)
        interval_list.push(main_chart.series[i].chart.eventOptions)
    }
})
//refresh chart
document.querySelector('#refresh').addEventListener('click', () => {
    current_symbol = document.querySelector('#stock-list').value
    get_stock_data(current_symbol)
        .then((stock_data) => {
            for (let id of interval_list) {
                clearInterval(id)
            }
            main_chart.destroy()

            let { close_price, date, test_close_price, test_date } = stock_data
            stock_data = restructure_data(close_price, date)
            let config = setup_config(stock_data, current_symbol)
            main_chart = Highcharts.stockChart('main_chart', config);

            let test_data = restructure_data(test_close_price, test_date)

            main_chart.series[0].chart.eventOptions = dynamic_load(main_chart.series[0], test_data)
            interval_list.push(main_chart.series[0].chart.eventOptions)
            // console.log(main_chart.series)
        })
    resetDefaultValue()
})


//listen to the chosen stock 
top_header.addEventListener('click', (e) => {
    if (e.target.matches('#stock-list')) {
        e.target.addEventListener('change', (e) => {
            let symbol = e.target.value
            current_symbol = symbol
            get_stock_data(symbol)
                .then((stock_data) => {
                    for (let id of interval_list) {
                        clearInterval(id)
                    }
                    main_chart.destroy()

                    let { close_price, date, test_close_price, test_date } = stock_data
                    stock_data = restructure_data(close_price, date)
                    let config = setup_config(stock_data, current_symbol)
                    main_chart = Highcharts.stockChart('main_chart', config);

                    let test_data = restructure_data(test_close_price, test_date)

                    main_chart.series[0].chart.eventOptions = dynamic_load(main_chart.series[0], test_data)
                    interval_list.push(main_chart.series[0].chart.eventOptions)
                    restartData = []
                    restartData.push(test_data)
                    // console.log(main_chart.series)
                })
            //delete user trade record and reset default value for event listener
            while (tbody.firstChild) {
                tbody.removeChild(tbody.firstChild);
            }
            resetDefaultValue()
        })
    }
})
//--------------------Event to add flag in the chart--------------------
Array.from(tradesList).forEach(target => {
    target.addEventListener('click', function (event) {
        // console.log(target)
        if (target == buy_button) {
            if (condition == 'buy') {
                window.alert("Please Sell Before Buy")
                return
            }
            addTradeRecord(target)
            pin_action('buy')

        }
        if (target == sell_button) {
            if (condition != 'buy') {
                window.alert("Please Buy Before Sell")
                return
            }
            addTradeRecord(target)
            pin_action('sell')
        }

    })
    // console.log('condition: ', condition)
});

//append column after click sell or buy button in trad record
buy_button.addEventListener('click', async () => {
    let tbody = document.querySelector('#user_record tbody')
    if (!predicted) {
        predicted = !predicted
        let picked_date = new Date(main_chart.series[0].processedXData.slice(-1)[0])
        // console.log('picked_date: ', picked_date);
        picked_date.setDate(picked_date.getDate() - 2)
        picked_date = format_date(picked_date)
        // console.log('picked_date: ', picked_date.setDate(main_chart.series[0].xData[0].getDate()));
        // console.log(picked_date)

        // console.log(main_chart.series[0])
        let predictions = await get_prediction(current_symbol, picked_date)

        for (let model in predictions) {
            let { predict_price, date } = predictions[model]
            let predict_data = restructure_data(predict_price, date)
            let previous_days = main_chart.series[0].xData.slice(0, 150)
            previous_days = previous_days.map(date => format_date(new Date(date)))
            // let previous_days = [];previous_days.length = 1000
            let previous_values = []
            for (let i in previous_days) {
                previous_values[i] = null
            }

            let previous_data = restructure_data(previous_values, previous_days)
            let first_inserted_data = predict_data.slice(0, 3)

            predict_data = predict_data.slice(3)

            let data = previous_data.concat(first_inserted_data)
            main_chart.addSeries({
                type: 'line',
                color: gen_color(model),
                id: `${model} - Predition`,
                name: `${model} - Predition`,
                data: data,
                tooltip: {
                    valueDecimals: 2
                }
            })
            restartData.push(predict_data)
        }

        for (let i = 1; i < restartData.length; i++) {
            main_chart.series[i].chart.eventOptions = dynamic_load(main_chart.series[i], restartData[i])
            interval_list.push(main_chart.series[i].chart.eventOptions)

            main_chart.series[i].chart.eventOptions = ai_sell(main_chart.series[i], restartData[i])
            interval_list.push(main_chart.series[i].chart.eventOptions)
        }
    }

})

// functions related to Symbol

async function load_symbol_list() {
    try {
        let result = await fetch('/symbol')
        let symbols = (await result.json()).data
        return symbols
    } catch (error) {
        console.log(error)
        return
    }
}

function insert_symbol_list(symbols) {
    let menu = drop_down_template.content.cloneNode(true)
    let list = menu.querySelector('#stock-list')
    let symbol_list = []
    for (let symbol of symbols) {
        let name = symbol['symbol']

        // return symbols
        let item = document.createElement('option')
        item.value = name
        item.textContent = name
        list.append(item)

        symbol_list.push(name)
        // nav_bar.prepend(menu)
        header_name.after(menu)
    }
    // nav_bar.prepend(menu)
    return symbol_list
}


async function get_stock_data(symbol) {
    try {
        let result = await fetch(`/stock/${symbol}`)
        let data = (await result.json()).data
        let { stock_name } = data
        // console.log (stock_data)
        let close_price = []
        let date = []
        let test_close_price = []
        let test_date = []

        let training_date = new Date(2018, 6, 26)
        // console.log (training_date)
        for (let row of data.stock_data) {
            let raw_date = new Date(row.date)
            // console.log (raw_date)

            if (raw_date >= training_date) {
                let formatted_date = format_date(raw_date)
                // console.log (formatted_date)
                test_close_price.push(row.adj_close)
                test_date.push(formatted_date)
            } else {
                let formatted_date = format_date(raw_date)
                close_price.push(row.adj_close)
                date.push(formatted_date)
            }
        }

        return { close_price, date, test_close_price, test_date }
    } catch (error) {
        console.log(error)
        return
    }
}

async function get_prediction(symbol, date) {
    try {
        let result = await fetch(`/prediction/${symbol}/${date}`)

        let data = (await result.json()).data
        // console.log(data)
        let predictions = {}
        for (let row of data) {
            let model = row.model
            let predict_price = []
            let date = []
            predictions[`${model}`] = { predict_price, date }
        }
        for (let row of data) {
            let model = row.model

            predictions[`${model}`].predict_price.push(row.predict_price)

            let raw_date = new Date(row.date)
            let formatted_date = `${raw_date.getFullYear()}-${raw_date.getMonth() + 1}-${raw_date.getDate()}`
            predictions[`${model}`].date.push(formatted_date)
        }
        // console.log(predictions)
        return predictions
    } catch (error) {
        console.log(error)
        return
    }
}


function restructure_data(price, raw_date) {
    stock_data = []
    for (let i in price) {
        let row = []
        let start_date = raw_date[i].split('-')
        let start_year = start_date[0]
        let start_month = start_date[1] - 1
        let start_day = start_date[2]

        row.push(Date.UTC(start_year, start_month, start_day))
        row.push(price[i])
        stock_data.push(row)
    }
    // console.log(stock_data)
    return stock_data
}

function format_date(date) {
    return date.getFullYear() + '-' + +(date.getMonth() + 1) + '-' + date.getDate()
}

function pin_action(action) {
    let labelColor
    let lineColor
    let textPosition
    if (action == 'buy') {
        labelColor = '#189AB4'
        lineColor = '#05445E'
        textPosition = 'right'
    } else {
        labelColor = '#FF0000'
        lineColor = '#A52A2A'
        textPosition = 'left'
    }
    main_chart.xAxis[0].addPlotBand({
        from: main_chart.series[0].processedXData.slice(-1)[0],
        to: main_chart.series[0].processedXData.slice(-1)[0],
        color: lineColor,
        id: 'plot-band-1',

        label: {
            text: `User ${action}`, // Content of the label. 
            textAlign: textPosition,
            style: { color: labelColor, fontSize: '1.2em' },

        }
    })

}

function pin_AI_action(model, action) {
    let labelColor
    let lineColor
    let textPosition
    let yPosition = 0
    if (model == "GAN" && action == 'buy') {
        labelColor = '#17C1C1'
        lineColor = '#05445E'
        textPosition = 'right'
        yPosition = 100
    } else if (model == "LSTM" && action == "buy") {
        labelColor = '#102770'
        lineColor = '#808000'
        textPosition = 'right'
        yPosition = 150
    }
    else if (model == "GAN" && action == "sell") {
        labelColor = '#9F4747'
        lineColor = '#808000'
        textPosition = 'left'
        yPosition = 200
    } else if (model == "LSTM" && action == "sell") {
        labelColor = '#700505'
        lineColor = '#808000'
        textPosition = 'left'
        yPosition = 250
    }
    main_chart.xAxis[0].addPlotBand({
        from: main_chart.series[0].processedXData.slice(-1)[0],
        to: main_chart.series[0].processedXData.slice(-1)[0],
        color: lineColor,
        id: 'plot-band-1',

        label: {
            text: `${model} ${action}`, // Content of the label. 
            textAlign: textPosition,
            style: { color: labelColor, fontSize: '1.2em' },
            y: yPosition

        }
    })

}

function addTradeRecord(target) {
    let date = new Date(main_chart.series[0].processedXData.slice(-1)[0])
    let tr = document.createElement("tr")
    let percentageChange
    const price_list = document.querySelectorAll('.price')
    const currentPrice = main_chart.series[0].processedYData.slice(-1)
    if (target == buy_button) {
        pin_AI_action('LSTM', 'buy')
        pin_AI_action('GAN', 'buy')
        dateToString = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
            ('0' + date.getDate()).slice(-2)
        tr.innerHTML = /* html */ `
    <tr>
        <td>${numberOfTrades}</td>
        <td>${dateToString}</td>
        <td>Buy</td>
        <td class="price">${currentPrice}</td>
        <td>--</td>
    </tr>
    `
        tbody.appendChild(tr)
        document.querySelector('#GAN_record tbody').innerHTML += tr.innerHTML
        document.querySelector('#LSTM_record tbody').innerHTML += tr.innerHTML
        condition = 'buy'
        numberOfTrades++
        LSTMTradenumber++
        GANTradenumber++
    }

    if (target == sell_button) {
        let percentageColor

        if (price_list.length <= 0) {
            percentageChange = '--'
        } else {
            //Last buy price
            let lastBuyPrice = parseFloat(document.querySelector('#user_record tbody').lastElementChild.querySelector('.price').textContent)
            percentageChange = Number(((currentPrice - lastBuyPrice) / lastBuyPrice) * 100).toPrecision(3)
            if (percentageChange > 0) {
                percentageColor = 'green'
            } else if (percentageChange == 0) {
                percentageColor = 'grey'
            } else {
                percentageColor = 'red'
            }
        }
        dateToString = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
            ('0' + date.getDate()).slice(-2)
        tr.innerHTML = /* html */ `
    <tr>
        <td>${numberOfTrades}</td>
        <td>${dateToString}</td>
        <td>Sell</td>
        <td class="price">${currentPrice}</td>
        <td style="color:${percentageColor};">${percentageChange}%</td>
    </tr>
    `
        tbody.appendChild(tr)
        numberOfTrades++
        condition = 'sell'
    }
}

function aiSellrecord(model) {
    let percentageChange
    let date = new Date(main_chart.series[0].processedXData.slice(-1)[0])
    let tr = document.createElement("tr")
    const price_list = document.querySelectorAll('.price')
    const currentPrice = main_chart.series[0].processedYData.slice(-1)
    if (model == 'LSTM') {
        pin_AI_action(model, 'sell')
        let lastBuyPrice = parseFloat(document.querySelector('#LSTM_record tbody').lastElementChild.querySelector('.price').textContent)
        percentageChange = Number(((currentPrice - lastBuyPrice) / lastBuyPrice) * 100).toPrecision(3)
        if (percentageChange > 0) {
            percentageColor = 'green'
        } else if (percentageChange == 0) {
            percentageColor = 'grey'
        } else {
            percentageColor = 'red'
        }
        dateToString = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
            ('0' + date.getDate()).slice(-2)
        tr.innerHTML = /* html */ `
    <tr>
        <td>${LSTMTradenumber}</td>
        <td>${dateToString}</td>
        <td>Sell</td>
        <td class="price">${currentPrice}</td>
        <td style="color:${percentageColor};">${percentageChange}%</td>
    </tr>
    `

        document.querySelector('#LSTM_record tbody').innerHTML += tr.innerHTML
        LSTMTradenumber++
    } else if (model == 'GAN') {
        pin_AI_action(model, 'sell')
        let lastBuyPrice = parseFloat(document.querySelector('#GAN_record tbody').lastElementChild.querySelector('.price').textContent)
        percentageChange = Number(((currentPrice - lastBuyPrice) / lastBuyPrice) * 100).toPrecision(3)
        if (percentageChange > 0) {
            percentageColor = 'green'
        } else if (percentageChange == 0) {
            percentageColor = 'grey'
        } else {
            percentageColor = 'red'
        }
        dateToString = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
            ('0' + date.getDate()).slice(-2)
        tr.innerHTML = /* html */ `
        <tr>
            <td>${GANTradenumber}</td>
            <td>${dateToString}</td>
            <td>Sell</td>
            <td class="price">${currentPrice}</td>
            <td style="color:${percentageColor};">${percentageChange}%</td>
        </tr>
        `
        document.querySelector('#GAN_record tbody').innerHTML += tr.innerHTML
        GANTradenumber++
    }


}

function ai_sell(series, data, time = 1000) {

    // console.log (values)
    // console.log (max_value)
    // console.log (series)
    series.sellPoint = 0
    series.model = series.name.replace(/ .*/, '')
    let model = series.model

    return setInterval(function () {
        data.shift()
        let predict_sell = data.slice(0, 60)
        let values = predict_sell.map(xy => xy[1])
        let max_value = Math.max(...values)

        // console.log (predict_sell[0][1])
        // console.log (max_value)
        // console.log (series.sellPoint)
        if (max_value > series.sellPoint) {
            console.log (series.model)
            console.log(`Original sell point of ${model} is ${series.sellPoint}.`)
            series.sellPoint = max_value
            predict_sell.map(xy => +(xy[1]) === max_value ? series.sellDate = xy[0] : series.sellDate)
            console.log(`New sell point of ${model} is ${max_value}.`)
            console.log(`New sell Date of ${model} is ${format_date(new Date(series.sellDate))}.`)
            console.log('')
        }

        // console.log('Model: ', series.model)
        // console.log('Current Date: ', series.processedXData.slice(-1))
        // console.log('Sell Point: ', series.sellPoint)
        // console.log('Current Price: ', series.processedYData.slice(-1))

        if (+(main_chart.series[0].processedXData.slice(-1)) == +(series.sellDate)) {
            aiSellrecord(model)
            // console.log(series)
            // console.log(main_chart.series[0].processedXData.slice(-1))
            // for (let i of interval_list) {
            //     clearInterval(i)
            // }
        }
    }, time);
}


async function get_Full_stock_data(symbol) {
    try {
        let result = await fetch(`/stock/${symbol}`)
        let data = (await result.json()).data
        let { stock_name } = data
        // console.log (stock_data)
        let close_price = []
        let date = []
        let test_close_price = []
        let test_date = []

        // console.log (training_date)
        for (let row of data.stock_data) {
            let raw_date = new Date(row.date)
            // console.log (raw_date)
            let formatted_date = format_date(raw_date)
            close_price.push(row.adj_close)
            date.push(formatted_date)
        }

        return { close_price, date }
    } catch (error) {
        console.log(error)
        return
    }
}

//load all data
document.querySelector('#show_answer').addEventListener('click', async () => {

    main_chart.destroy()

    //load stock price
    current_symbol = top_header.querySelector('#stock-list').value
    stock_data = await get_Full_stock_data(current_symbol)
    let { close_price, date } = stock_data
    stock_data = restructure_data(close_price, date)
    let config = setup_config(stock_data, current_symbol)
    main_chart = Highcharts.stockChart('main_chart', config);


    //load prediction
    let picked_date = new Date(main_chart.series[0].xData[0])
    picked_date.setDate(picked_date.getDate())
    picked_date = format_date(picked_date)
    let predictions = await get_prediction(current_symbol, picked_date)
    console.log('predictions: ', predictions);
    for (let model in predictions) {
        let { predict_price, date } = predictions[model]
        let predict_data = restructure_data(predict_price, date)
        console.log(predict_data)

        main_chart.addSeries({
            type: 'line',
            color: gen_color(model),
            id: `${model} - Predition`,
            name: `${model} - Predition`,
            data: predict_data,
            tooltip: {
                valueDecimals: 2
            }
        })
        // console.log('main_chart.series',main_chart.series)
    }
})