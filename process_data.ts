import { readdirSync } from "fs";
import { join } from 'path';



export function trim_suffix(path: string) {
    let trimmed_list: string[] = [];

    let files = readdirSync(path)

    files.forEach((file) => {
        // console.log('...Trimming: ', file)
        let symbol = file.replace(/\..*/, '')
        trimmed_list.push(symbol)
        // console.log('...Trimmed and Inserted: ', symbol)
    })
    return trimmed_list
}


export const stocks_list_path = join('data')
export const trimmed_symbol_list = trim_suffix(stocks_list_path)

export const predictions_path = join('predictions')
export let prediction_list  = trim_suffix(predictions_path).map(row => {
    let arr = row.split('_')
    return arr
})


// for (let symbol of trimmed_symbol_list) {
// console.log ()
// }


// import { readFile, utils } from 'xlsx'
// import { Stock } from "./type";
// import { format } from 'date-fns'

// let wb = readFile(join(stocks_list_path, `KSU.csv`), { cellDates: true })
// let sheet_name = wb.SheetNames[0]
// let data: Stock[] = utils.sheet_to_json(wb.Sheets[sheet_name])

// let row = data[(data.length) - 1]

// console.log(!row['Open'])
// console.log(!row['High'])
// console.log(!row['Low'])
// console.log(!row['Close'])
// console.log(!row['Adj Close'])
// loop each row in the symbol csv-json data

// for (let row of data) {
// row['Date'] = format(new Date(row['Date']), 'yyyy-MM-dd')
// row['Open'] = +row['Open'].toFixed(2)
// row['High'] = +row['High'].toFixed(2)
// row['Low'] = +row['Low'].toFixed(2)
// row['Close'] = +row['Close'].toFixed(2)
// row['Adj Close'] = +row['Adj Close'].toFixed(2)

// console.log(row)
    // }

