import { Request, Response, Router } from "express";
import { Prediction_Service } from "./prediction.service";

export class Prediction_Controller {
    router = Router()
    constructor(private prediction_service: Prediction_Service) {
        this.router.get('/prediction/:symbol/:date', this.load_prediction)
    }
    load_prediction = async (req: Request, res:Response)=>{
        let {symbol, date} = req.params
        try {
            let prediction = await this.prediction_service.load_prediction(symbol, date)
            res.status(200).json({ message: 'The data is Successfully loaded.', data: prediction})
        } catch (error) {
            res.status(500).json(error)
        }
    }

}
