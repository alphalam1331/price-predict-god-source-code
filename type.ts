export type Stock = {
    Date: string,
    Open: number,
    High: number,
    Low: number,
    Close: number,
    'Adj Close': number,
    Volume: number
}

export type Pattern = {
    symbol_name: string
    , date1: string
    , price1: number
    , date2: string
    , price2: number
    , pattern_label: string
}

export type Prediction = {
    Date: string, 
    Close: number, 
    Predictions: number
}