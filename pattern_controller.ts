import { Request, Response, Router } from "express";
import { Pattern_Service } from "./pattern_service";
import { Pattern } from "./type";

export class Pattern_Controller {
    router = Router()
    constructor(private pattern_service: Pattern_Service) {
        this.router.get('/pattern', this.load_all_patterns)
        this.router.post('/pattern', this.submit_pattern)
    }
    load_all_patterns = async(req: Request, res: Response) => {
        try {
            let pattern_list = await this.pattern_service.load_all_patterns()
            res.status(200).json({ message: 'The data is Successfully loaded.', data: pattern_list})
        } catch(error){
            res.status(500).json(error)
        }
    }

    submit_pattern = (req: Request, res: Response) => {
        let picked_pattern: Pattern = req.body
        try {
            let result = this.pattern_service.submit_pattern(picked_pattern)
            res.status(200).json({
                message: `Successfully receiving data`
                , data: result
            })
        } catch (error) {
            res.status(500).json(error)
        }
    }
}