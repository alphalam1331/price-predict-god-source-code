import { print } from 'listening-on'
import { env } from './env'
import express from "express";
import path from 'path'
import { Stock_Service } from './stock_service';
import { Stock_Controller } from './stock_controller';
import { knex } from './knex';
import { Pattern_Service } from './pattern_service';
import { Pattern_Controller } from './pattern_controller';
import { Prediction_Service } from './prediction.service';
import { Prediction_Controller } from './prediction.controller';

const app = express()
const stock_service = new Stock_Service(knex)
const stock_controller = new Stock_Controller(stock_service)

const pattern_service = new Pattern_Service(knex)
const pattern_controller = new Pattern_Controller(pattern_service)

const prediction_service = new Prediction_Service(knex)
const prediction_controller = new Prediction_Controller(prediction_service)

app.use(express.urlencoded({ extended: false }))
app.use(express.json())

app.use(stock_controller.router)
app.use(pattern_controller.router)
app.use(prediction_controller.router)

app.use(express.static('./public'))

app.listen(env.PORT, () => {
    print(env.PORT)
})


app.use((req, res) => {
    res.status(404).sendFile(path.resolve(path.join('public', '404.html')))
})