stocks
-
symbol string pk

stocks_data
-
id pk
date date
symbol string index fk >- stocks.symbol
open float
high float 
low float
close float
adj_close float
volume bigint

patterns
-
id pk
pattern_name unique

pattern_data
-
id pk
pattern_id int fk >- patterns.id
start_point int fk >- stocks_data.id
end_point int fk >- stocks_data.id

prediction
-
id pk
date date
symbol string index fk >- stocks.symbol
predict_price float
model string fk >- model.name

model
-
name string pk