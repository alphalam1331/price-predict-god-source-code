import { Knex } from 'knex';

export class Stock_Service {
    constructor(private knex: Knex) { }

    load_all_symbols = async () => {
        try {
            let symbol_list = (await this.knex('stocks').select('symbol'))
            return symbol_list
        } catch (error) {
            return Error('failed to load symbol.')
        }
    }
    load_stock_data = async (symbol: string, ) => {
        try {
            let stock_data = await this.knex('stocks_data').select('date', 'adj_close').where('symbol', symbol).orderBy('date', 'asc')
            // console.log ('Loading Stock Data: ', stock_data)
            return { symbol, stock_data }
        }
        catch (error){
            console.log (error)
            return Error('failed to load stock data.')
        }
    }
}

