import { Knex } from "knex";
import { readFile, utils } from 'xlsx'
import { join } from 'path';
import { trimmed_symbol_list, stocks_list_path } from "../process_data";
import { Stock } from "../type";
import { format } from 'date-fns'


export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    // Inserts seed entries

    for (let symbol of trimmed_symbol_list) {
            await knex("stocks").insert(
                { symbol: symbol }
            )
            let wb = readFile(join(stocks_list_path, `${symbol}.csv`), { cellDates: true })
            let sheet_name = wb.SheetNames[0]
            let data: Stock[] = utils.sheet_to_json(wb.Sheets[sheet_name])

            // loop each row in the symbol csv file
            for (let row of data) {
                row['Date'] = row['Date'] ? format(new Date(row['Date']), 'yyyy-MM-dd') : ''
                row['Open'] = row['Open'] ? +row['Open'].toFixed(2) : NaN
                row['High'] = row['High'] ? +row['High'].toFixed(2) : NaN
                row['Low'] = row['Low'] ? +row['Low'].toFixed(2) : NaN
                row['Close'] = row['Close'] ? +row['Close'].toFixed(2) : NaN
                row['Adj Close'] = row['Adj Close'] ? +row['Adj Close'].toFixed(2) : NaN

                await knex("stocks_data").insert(
                    {
                        symbol: symbol
                        , date: row['Date']
                        , open: row['Open']
                        , high: row['High']
                        , low: row['Low']
                        , close: row['Close']
                        , adj_close: row['Adj Close']
                        , volume: row['Volume']
                    },
                );
            }
    }
}